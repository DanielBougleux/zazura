from Menus.Button import *
from PPlay.mouse import *
from PPlay.gameimage import *


class MenuP:

    def __init__(self, janela):
        self.janela = janela

        self.fundo = GameImage("Graphics/Menu/fundo.jpg")
        self.fundo.set_position((janela.width - self.fundo.width)/2, (janela.height - self.fundo.height)/2)

        self.menuf = GameImage("Graphics/Menu/menu.png")
        self.menuf.set_position(self.fundo.x + 20, self.fundo.y + (self.fundo.height - self.menuf.height)/2)

        self.buttons = []

        self.novojogo = Button(("Graphics/Menu/novo-jogosf.png", "Graphics/Menu/novo-jogo.png"), 1)
        coordx = self.menuf.x + (self.menuf.width - self.novojogo.width)/2
        espacamentoy = (self.menuf.height - self.novojogo.height*4)/5
        self.novojogo.setposition(coordx, self.menuf.y + espacamentoy)
        self.buttons.append(self.novojogo)

        self.carregar = Button(("Graphics/Menu/carregarsf.png", "Graphics/Menu/carregar.png"), 2)
        self.carregar.setposition(coordx, self.menuf.y + espacamentoy*2 + self.novojogo.height)
        self.buttons.append(self.carregar)

        self.controles = Button(("Graphics/Menu/controlessf.png", "Graphics/Menu/controles.png"), 3)
        self.controles.setposition(coordx, self.menuf.y + espacamentoy*3 + self.novojogo.height*2)
        self.buttons.append(self.controles)

        self.sair = Button(("Graphics/Menu/sairsf.png", "Graphics/Menu/sair.png"), 4)
        self.sair.setposition(coordx, self.menuf.y + espacamentoy*4 + self.novojogo.height*3)
        self.buttons.append(self.sair)
        self.mouse = Mouse()
        self.mouseState = False
        self.code = 0

    def loop(self):

        self.mouseState = False
        self.code = 0

        while True:

            for i in self.buttons:
                clicou = i.update()
                if clicou:
                    self.mouseState = True
                    self.code = i.code

            if self.mouseState and not self.mouse.is_button_pressed(1):
                self.janela.clear()
                return self.code

            self.render()
            self.janela.update()

    def render(self):
        self.fundo.draw()
        self.menuf.draw()
        for i in self.buttons:
            i.render()
