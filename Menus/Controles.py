from PPlay.gameimage import *
from PPlay.keyboard import *


class Controles:

    def __init__(self, janela):

        self.janela = janela

        self.fundo = GameImage("Graphics/Menu/fundo.jpg")
        self.fundo.set_position((self.janela.width - self.fundo.width)/2, (self.janela.height - self.fundo.height)/2)

        self.controles = GameImage("Graphics/Menu/pcontroles2.png")
        self.controles.set_position((self.janela.width - self.controles.width)/2,
                                    (self.janela.height - self.controles.height)/2 + 70)
        self.teclado = Keyboard()

    def loop(self):

        while True:
            if self.teclado.key_pressed("ESC"):
                self.janela.clear()
                return

            self.render()
            self.janela.update()

    def render(self):
        self.fundo.draw()
        self.controles.draw()