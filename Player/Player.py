from Player.Skills.Skills import *
from PPlay.keyboard import *


class Player:

    def __init__(self, janela, attackspeed, hp, ataque, defesa):

        self.janela = janela
        self.teclado = Keyboard()
        self.velocidade_ataque = attackspeed
        self.sprites = []
        self.tempo_ataque = 0
        self.sem_fisica = False
        self.contador_fisica = 0

        self.hp = hp
        self.danoataque = ataque
        self.defesa = defesa

        self.esp = 0
        self.tempo_especialz = 7
        self.tempo_especialx = 10
        self.cooldownz = 7
        self.cooldownx = 10
        self.cooldownc = 5
        self.z = False
        self.x = False
        self.c = False
        self.tempo_de_animaz = 0
        self.espat = False

        self.parado = Sprite("Graphics/Player/Parado.png")
        self.sprites.append(self.parado)
        self.paradoesq = Sprite("Graphics/Player/Parado1.png")
        self.sprites.append(self.paradoesq)
        self.pulo_up_dir = Sprite("Graphics/Player/puloupdir.png")
        self.sprites.append(self.pulo_up_dir)
        self.pulo_up_esq = Sprite("Graphics/Player/puloupesq.png")
        self.sprites.append(self.pulo_up_esq)
        self.pulo_down_dir = Sprite("Graphics/Player/pulodowndir.png")
        self.sprites.append(self.pulo_down_dir)
        self.pulo_down_esq = Sprite("Graphics/Player/pulodownesq.png")
        self.sprites.append(self.pulo_down_esq)
        self.corridadir = Sprite("Graphics/Player/Corrida.png", 4)
        self.sprites.append(self.corridadir)
        self.corridaesq = Sprite("Graphics/Player/corridaesq.png", 4)
        self.sprites.append(self.corridaesq)
        self.ataque = Sprite("Graphics/Player/ataque.png", 3)
        self.sprites.append(self.ataque)
        self.ataqueesq = Sprite("Graphics/Player/bateresq.png", 3)
        self.sprites.append(self.ataqueesq)
        self.colisor = Sprite("Graphics/Player/colisor.png")

        self.set_position(10, 450)

        self.ataque.set_loop(False)
        self.ataqueesq.set_loop(False)
        self.corridadir.set_total_duration(400)
        self.corridaesq.set_total_duration(400)
        self.ataque.set_total_duration(400)
        self.ataqueesq.set_total_duration(400)

        self.duracao_ataque = 0

        self.noar = False

        self.velocidadeplayerx = 200
        self.velocidadeplayery = -420

        self.velocidadex = self.velocidadeplayerx
        self.velocidadey = 0
        self.fallrate = 1

        self.is_attacking = False
        self.bloqueiadash = False
        self.contadash = self.cooldownc
        self.is_dashing = False
        self.in_ground = False
        self.is_turned_right = True
        self.clicouup = False
        self.ismovingright = False
        self.ismovingleft = False
        self.isblocked = False
        self.got_hit = False
        self.conta_invul = 0

        self.player = self.parado

        self.coolz = []
        self.coolc = []

        for i in range(10):
            self.coolz.append(Sprite("Graphics/Player/Cooldown/z/cooldown{}.png".format(i)))

        for i in range(5):
            self.coolc.append(Sprite("Graphics/Player/Cooldown/c/cooldown{}.png".format(i)))
            self.coolc[i].set_position(50, 0)

    def update(self):

        if self.noar:
            self.gravity()
        else:
            self.fallrate = 300

        if self.teclado.key_pressed("Z") and self.tempo_especialz > self.cooldownz and not self.espat:
            self.esp = Skills(self.janela, self.player, "z", 150)
            self.espat = True
            self.tempo_especialz = 0

        if self.teclado.key_pressed("X") and self.tempo_especialx > self.cooldownx and not self.noar and not self.espat:
            self.esp = Skills(self.janela, self.player, "x", 120)
            self.espat = True
            self.x = True
            self.tempo_especialx = 0

        if self.teclado.key_pressed("SPACE") and self.tempo_ataque > self.velocidade_ataque and not self.espat:
            self.is_attacking = True
            self.ataque.play()
            self.ataqueesq.play()
            self.tempo_ataque = 0
            self.duracao_ataque = 0

        if self.teclado.key_pressed("UP") and not self.noar and not self.clicouup and not self.espat:
            self.velocidadey = self.velocidadeplayery
            self.clicouup = True
            self.noar = True

        if self.clicouup and not self.teclado.key_pressed("UP") and not self.espat:
            self.clicouup = False

        if self.teclado.key_pressed("DOWN") and not self.in_ground and not self.noar and not self.is_dashing and not self.espat:
            self.noar = True
            self.sem_fisica = True
            self.contador_fisica = 0

        self.movimentoy()

        if self.teclado.key_pressed("C") and not self.bloqueiadash and not self.espat:
            if self.is_turned_right:
                self.set_position(self.colisor.x + (1000 * self.janela.delta_time()), self.player.y)
            else:
                self.set_position(self.colisor.x - (1000 * self.janela.delta_time()), self.player.y)
            self.contadash += 1000 * self.janela.delta_time()
            self.is_dashing = True
            if self.contadash > 200:
                self.bloqueiadash = True
                self.contadash = 0
        else:
            self.is_dashing = False

        if self.teclado.key_pressed("RIGHT") and not self.espat:

            self.player = self.corridadir
            self.corre(self.velocidadex)
            self.is_turned_right = True
            self.ismovingright = True
            self.ismovingleft = False
            self.corridadir.update()

        elif self.teclado.key_pressed("LEFT") and not self.espat:

            self.player = self.corridaesq
            self.corre(self.velocidadex * -1)
            self.is_turned_right = False
            self.ismovingleft = True
            self.ismovingright = False
            self.corridaesq.update()

        else:
            if self.is_turned_right:
                self.player = self.parado
            else:
                self.player = self.paradoesq

            self.corridadir.set_curr_frame(0)
            self.corridaesq.set_curr_frame(0)
            self.ismovingleft = False
            self.ismovingright = False

        if self.noar:

            if self.velocidadey < 0 and self.is_turned_right:
                self.player = self.pulo_up_dir
            if self.velocidadey < 0 and not self.is_turned_right:
                self.player = self.pulo_up_esq
            if self.velocidadey > 0 and self.is_turned_right:
                self.player = self.pulo_down_dir
            if self.velocidadey > 0 and not self.is_turned_right:
                self.player = self.pulo_down_esq

        if self.is_attacking:
            if self.is_turned_right:
                self.player = self.ataque
            else:
                self.player = self.ataqueesq
            self.ataque.update()
            self.ataqueesq.update()
            if not self.ataque.playing and self.duracao_ataque > 0.4:
                self.ataque.set_curr_frame(0)
                self.ataqueesq.set_curr_frame(0)
                self.is_attacking = False

        if self.sem_fisica:

            if self.contador_fisica > 0.5:
                self.sem_fisica = False

        if self.bloqueiadash:
            if self.contadash > self.cooldownc:
                self.bloqueiadash = False

        if self.got_hit and self.conta_invul > 1:
            self.got_hit = False

        if not self.x and not self.c:
            if self.espat and self.esp.especialz.get_curr_frame() > 2:
                self.z = True
                self.tempo_de_animaz = 0
        elif self.espat and self.x:
            self.z = True
            self.tempo_de_animaz = 0
        if self.z:
            self.tempo_de_animaz += self.janela.delta_time()
        if self.tempo_de_animaz > 4:
            self.z = False
            self.x = False

        self.tempo_especialz += self.janela.delta_time()
        self.tempo_especialx += self.janela.delta_time()
        self.conta_invul += self.janela.delta_time()
        self.tempo_ataque += self.janela.delta_time()
        self.duracao_ataque += self.janela.delta_time()
        self.contador_fisica += self.janela.delta_time()
        self.contadash += self.janela.delta_time()

    def render(self):
        self.player.draw()
        self.colisor.draw()

    def corre(self, velocidade):

        for i in self.sprites:
            i.move_x(velocidade * self.janela.delta_time())
        self.colisor.move_x(velocidade * self.janela.delta_time())

    def movimentoy(self):

        for i in self.sprites:
            i.move_y(self.velocidadey * self.janela.delta_time())
        self.colisor.move_y(self.velocidadey * self.janela.delta_time())

    def gravity(self):

        self.velocidadey += self.fallrate * self.janela.delta_time()
        self.fallrate += 3000 * self.janela.delta_time()

    def set_position(self, x, y):

        for i in self.sprites:
            i.set_position(x, y)

        self.ataqueesq.set_position(x - 70, y)
        self.colisor.set_position(self.parado.x, self.parado.y + 19)

    def ilusao(self, mode):

        if mode:
            self.velocidadex = 0
        else:
            self.velocidadex = self.velocidadeplayerx

    def caiu(self, caiu, posy=0):

        if caiu:
            self.noar = True
        else:
            self.set_position(self.colisor.x, posy - self.player.height)
            self.noar = False
            self.velocidadey = 0
            self.fallrate = 1

    def hit(self, dano):
        self.got_hit = True
        self.conta_invul = 0
        self.hp -= dano

    def verifica_hit(self, monstro):

        if self.is_attacking:
            if monstro.in_range(self):
                if not monstro.got_hit:

                    if monstro.colisor.x > self.colisor.x and self.is_turned_right:
                        if monstro.colisor.x < self.colisor.x + 100:
                            return True

                    if monstro.colisor.x < self.colisor.x and not self.is_turned_right:
                        if monstro.colisor.x + monstro.colisor.width > self.colisor.x + self.colisor.width - 100:
                            return True

        return False

    def desenha_cooldown(self):

        z = int(self.cooldownz - self.tempo_especialz)
        c = int(self.cooldownc - self.contadash)

        if z < 0:
            self.coolz[0].draw()
        else:
            self.coolz[z].draw()

        if c < 0:
            self.coolc[0].draw()
        else:
            self.coolc[c].draw()
