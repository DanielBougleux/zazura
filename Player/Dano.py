from PPlay.sprite import *


class Dano:

    def __init__(self, dano, x, y, janela):

        aux = str(dano)
        self.dano = []
        self.janela = janela
        self.duracao = 1
        self.velocidade = -90
        self.contador = 0
        self.finished = False
        if len(aux) == 1:
            self.dano.append(Sprite("Graphics/Dano/0.png"))
            self.dano.append(Sprite("Graphics/Dano/{}.png".format(aux[0])))
        else:
            self.dano.append(Sprite("Graphics/Dano/{}.png".format(aux[0])))
            self.dano.append(Sprite("Graphics/Dano/{}.png".format(aux[1])))

        self.dano[0].set_position(x, y)
        self.dano[1].set_position(x + self.dano[0].width, y)

    def update(self):

        for i in self.dano:
            i.move_y(self.velocidade * self.janela.delta_time())

        if self.contador > self.duracao:
            self.finished = True

        self.contador += self.janela.delta_time()

    def render(self):

        for i in self.dano:
            i.draw()
