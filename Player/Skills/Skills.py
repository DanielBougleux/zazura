from PPlay.sprite import *


class Skills:

    def __init__(self, janela, player, skill, damage):
        self.janela = janela
        self.player = player
        self.skill = skill
        self.damage = damage
        self.duracao_especial = 0
        if skill == "z":
            self.especialz = Sprite("Graphics/Player/especialdir1.png", 8)
            self.especialz.set_loop(False)
            self.especialz.set_total_duration(800)
            self.especialz.play()
            self.especial = Sprite("Graphics/Player/Skills/Z/especialzdir.png", 8)
            self.especial.set_total_duration(400)
            self.especial.set_position(player.x-5, player.y+15)
        if skill == "x":
            self.especialx = Sprite("Graphics/Player/especialesf.png", 2)
            self.especialx.set_total_duration(400)
            self.especial = Sprite("Graphics/Player/Skills/x/especialx.png",25)
            self.especial.set_total_duration(2000)
            self.especial.set_position(player.x-self.especial.width/2+20, player.y-self.especial.height/2+40)
            self.especial.set_loop(False)

    def moveskill(self,velocidade):
        self.especial.move_x(velocidade * self.janela.delta_time())

    def update(self, atv, espat):
        if self.skill == "z":
            if espat:
                if atv:
                    self.moveskill(400)
                    self.especial.update()
                self.especialz.update()
                self.duracao_especial += self.janela.delta_time()
                if not self.especialz.playing and self.duracao_especial > 0.8:
                    self.especialz.set_curr_frame(0)
                    self.duracao_especial = 0
                    return False
                return True
            elif atv:
                self.moveskill(400)
                self.especial.update()
                return False

        if self.skill == "x":
            self.especial.update()
            if espat:
                self.especialx.update()
                self.duracao_especial += self.janela.delta_time()
                if self.duracao_especial > 3:
                    self.especialx.set_curr_frame(0)
                    self.duracao_especial =0
                    return False
                return True
            return False

    def render(self, atv, espat):
        if atv :
            if self.skill=="z":
                self.especial.draw()
        if self.skill=="x" and espat:
            self.especialx.set_position(self.player.x, self.player.y + 20)
            self.especial.draw()
            self.especialx.draw()

        if self.skill=="z" and espat:
            self.especialz.set_position(self.player.x - 28, self.player.y - 24)
            self.especialz.draw()