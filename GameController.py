from Menus.MenuP import *
from Menus.Controles import *
from Fases.Fase import *
from PPlay.window import *
from Player.Player import *

janela = Window(800, 600)
janela.set_title("ZAZURA")
MenuP = MenuP(janela)
jogador = Player(janela, 1, 500, 100, 50)
Fase2 = Fase(janela, jogador, 2)
Controles = Controles(janela)

while True:

    op = MenuP.loop()
    if op == 1:
        Fase2.loop()
    if op == 3:
        Controles.loop()
    if op == 4:
        exit(0)
