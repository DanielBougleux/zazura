from PPlay.sprite import *
from random import randint


class Item:

    def __init__(self, x, y):

        self.item = Sprite("Graphics/Extras/item.png")
        self.item.set_position(x, y - self.item.height)
        self.efeito = randint(0, 2)

    def update(self, player):

        if self.item.collided(player.colisor) and player.is_attacking:
            self.aplicaEfeito(player)
            return True
        return False

    def aplicaEfeito(self, player):
        if self.efeito == 0:
            player.hp += 200
        elif self.efeito == 1:
            player.danoataque += 10
        elif self.efeito == 2:
            player.defesa += 10

    def render(self):
        self.item.draw()
