from PPlay.sprite import *


class Arvore:

    def __init__(self, plataforma, janela):

        self.sprites = []

        self.danoataque = 100
        self.defesa = 50
        self.hp = 400

        self.parado = Sprite("Graphics/Monstros/Arvore/static1.png", 3)
        self.parado.set_total_duration(400)
        self.sprites.append(self.parado)

        self.paradoesq = Sprite("Graphics/Monstros/Arvore/staticesq.png", 3)
        self.paradoesq.set_total_duration(400)
        self.sprites.append(self.paradoesq)

        self.corridadir = Sprite("Graphics/Monstros/Arvore/corrida2.png", 3)
        self.corridadir.set_total_duration(400)
        self.sprites.append(self.corridadir)

        self.corridaesq = Sprite("Graphics/Monstros/Arvore/Corrida3.png", 3)
        self.corridaesq.set_total_duration(400)
        self.sprites.append(self.corridaesq)

        self.ataque = Sprite("Graphics/Monstros/Arvore/ataque.png", 22)
        self.ataque.set_total_duration(800)
        self.ataque.set_loop(False)
        self.sprites.append(self.ataque)

        self.ataqueesq = Sprite("Graphics/Monstros/Arvore/ataqueesq.png", 22)
        self.ataqueesq.set_total_duration(800)
        self.ataqueesq.set_loop(False)
        self.sprites.append(self.ataqueesq)

        self.monstro = self.parado

        self.colisor = Sprite("Graphics/Monstros/Arvore/Colisor.png")

        self.janela = janela

        self.plataforma = plataforma
        self.set_position(self.plataforma.x, self.plataforma.y - self.monstro.height)

        self.vel_ataque = 2

        self.duracao_comportamento = 4
        self.timer_comportamento = 0
        self.timer_ataque = 0
        self.got_hit = False
        self.conta_invul = 0
        self.timer_final_frame = 0
        self.comportamento = 0
        self.is_attacking = False
        self.is_turned_right = True
        self.velocidadex = 60

    def update(self, player, controladorplataformas):

        if self.timer_comportamento > self.duracao_comportamento:
            self.comportamento += 1
            self.timer_comportamento = 0
            if self.comportamento > 2:
                self.comportamento = 0

        if self.in_range(player):
            if player.player.x < self.colisor.x:
                if player.player.x > self.colisor.x - 100 and self.timer_ataque > self.vel_ataque:
                    self.is_attacking = True
                    self.ataque.play()
                    self.ataqueesq.play()
                    self.timer_ataque = 0
                    self.timer_final_frame = 0
                else:
                    self.comportamento = 2
            elif player.player.x > self.colisor.x:
                if player.player.x < self.colisor.x + 100 and self.timer_ataque > self.vel_ataque:
                    self.is_attacking = True
                    self.timer_ataque = 0
                    self.ataque.play()
                    self.ataqueesq.play()
                    self.timer_final_frame = 0
                else:
                    self.comportamento = 1

        if self.colisor.x < self.plataforma.x:
            self.velocidadex = 0
            if self.comportamento == 1:
                self.set_position(self.monstro.x + 3, self.monstro.y)
            else:
                self.comportamento = 0

        elif self.colisor.x + self.colisor.width > self.plataforma.x + self.plataforma.width:
            self.velocidadex = 0
            if self.comportamento == 2:
                self.set_position(self.monstro.x - 3, self.monstro.y)
            else:
                self.comportamento = 0

        else:
            self.velocidadex = 60

        if controladorplataformas.ismovingright:
            self.move(player.velocidadeplayerx)
        elif controladorplataformas.ismovingleft:
            self.move(-player.velocidadeplayerx)

        if not self.is_attacking:
            self.natural_move()
        else:
            if self.is_turned_right:
                self.monstro = self.ataque
            else:
                self.monstro = self.ataqueesq
            self.ataqueesq.update()
            self.ataque.update()

            if not self.monstro.is_playing() and self.timer_final_frame > 0.6:
                self.ataque.set_curr_frame(0)
                self.ataqueesq.set_curr_frame(0)
                self.is_attacking = False

        if self.got_hit and self.conta_invul > 1:
            self.got_hit = False

        self.conta_invul += self.janela.delta_time()
        self.timer_comportamento += self.janela.delta_time()
        self.timer_final_frame += self.janela.delta_time()
        self.timer_ataque += self.janela.delta_time()

    def render(self):

        self.monstro.draw()
        self.colisor.draw()

    def move(self, velocidade):

        for i in self.sprites:
            i.move_x(velocidade * self.janela.delta_time())
        self.colisor.move_x(velocidade * self.janela.delta_time())

    def natural_move(self):

        if self.comportamento == 0:
            if self.is_turned_right:
                self.monstro = self.parado
            else:
                self.monstro = self.paradoesq
            self.parado.update()
            self.paradoesq.update()
            self.corridadir.set_curr_frame(0)
            self.corridaesq.set_curr_frame(0)

        if self.comportamento == 1:

            self.is_turned_right = True
            self.monstro = self.corridadir
            self.corridadir.update()
            self.move(self.velocidadex)

        if self.comportamento == 2:

            self.is_turned_right = False
            self.monstro = self.corridaesq
            self.corridaesq.update()
            self.move(-self.velocidadex)

    def set_position(self, x, y):

        for i in self.sprites:
            i.set_position(x, y)
        self.colisor.set_position(x + 68, y + 47)

    def in_range(self, player):

        if self.colisor.y + self.colisor.height > player.colisor.y > self.colisor.y:
            return True

        return False

    def hit(self, dano):
        self.got_hit = True
        self.conta_invul = 0
        self.hp -= dano

    def verifica_hit(self, player):

        if self.is_attacking:

            if self.in_range(player):

                if not player.got_hit:
                    if player.colisor.x > self.colisor.x and self.is_turned_right:
                        if player.colisor.x < self.colisor.x + 130:
                            if 12 <= self.ataque.get_curr_frame() <= 17:
                                return True

                    if player.colisor.x < self.colisor.x and not self.is_turned_right:
                        if player.colisor.x + player.colisor.width > self.colisor.x + self.colisor.width - 130:
                            if 12 <= self.ataque.get_curr_frame() <= 17:
                                return True

        return False
