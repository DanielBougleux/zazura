from Extras.Item import *


class ControladorItens:

    def __init__(self, player, janela, controlador_plataformas):

        self.controlador_plataformas = controlador_plataformas
        self.janela = janela
        self.itens = []
        self.mensagens = []
        self.atkadd = 0
        self.defadd = 0
        self.player = player

    def update(self):

        for i in self.itens:
            pegou = i.update(self.player)
            if pegou:
                if i.efeito == 0:
                    self.mensagens.append(Sprite("Graphics/Extras/HpUp.png"))
                    self.mensagens[len(self.mensagens)-1].set_position(self.player.colisor.x,
                                                                       self.player.colisor.y)

                if i.efeito == 1:
                    self.mensagens.append(Sprite("Graphics/Extras/AtkUp.png"))
                    self.mensagens[len(self.mensagens) - 1].set_position(self.player.colisor.x,
                                                                         self.player.colisor.y)
                    self.atkadd += 10

                if i.efeito == 2:
                    self.mensagens.append(Sprite("Graphics/Extras/DefUp.png"))
                    self.mensagens[len(self.mensagens) - 1].set_position(self.player.colisor.x,
                                                                         self.player.colisor.y)
                    self.defadd += 10

                self.itens.remove(i)
                break

        for i in self.mensagens:
            i.move_y(-100 * self.janela.delta_time())
            if i.y < 0:
                self.mensagens.remove(i)
                break

        self.move()

    def add_item(self, x, y):

        self.itens.append(Item(x, y))

    def move(self):

        velocidade = 0

        if self.controlador_plataformas.ismovingright:

            velocidade = self.player.velocidadeplayerx

        elif self.controlador_plataformas.ismovingleft:

            velocidade = -self.player.velocidadeplayerx

        for i in self.itens:
            i.item.move_x(velocidade * self.janela.delta_time())

        for i in self.mensagens:
            i.move_x(velocidade * self.janela.delta_time())

    def render(self):

        for i in self.itens:
            i.render()

        for j in self.mensagens:
            j.draw()
