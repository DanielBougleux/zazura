from PPlay.sprite import *


class ControladorPlataformas:

    def __init__(self, arquivoposplat, arquivoposchao, diretorio, janela):

        self.janela = janela
        self.plataformas = []
        self.chao = []
        self.ismovingleft = False
        self.ismovingright = False
        self.limiteesq = True
        self.limitedir = False

        arqplat = open(arquivoposplat, 'r')
        linhas = arqplat.readlines()
        arqplat.close()

        arqchao = open(arquivoposchao, 'r')
        poschao = arqchao.readlines()
        arqchao.close()

        for i in linhas:

            i = i.split(" ")

            if "\n" in i[2]:
                i[2] = i[2][:len(i[2]) - 1]

            self.plataformas.append(Sprite("{}/plat{}.png".format(diretorio, i[0])))
            self.plataformas[len(self.plataformas) - 1].set_position(int(i[1]), int(i[2]))

        for i in poschao:

            i = i.split(" ")

            if "\n" in i[2]:
                i[2] = i[2][:len(i[2]) - 1]

            self.chao.append(Sprite("{}/chão{}.png".format(diretorio, i[0])))
            self.chao[len(self.chao) - 1].set_position(int(i[1]), int(i[2]))

    def update(self, player):

        self.definelimite()

        if player.player.x > self.janela.width/2:

            if player.ismovingright and not self.limitedir:
                player.ilusao(True)
                self.ismovingleft = True
                self.move(-player.velocidadeplayerx)
            else:
                player.ilusao(False)
                self.ismovingleft = False
        else:
            self.ismovingleft = False

        if player.player.x < self.janela.width/2:

            if player.ismovingleft and not self.limiteesq:
                player.ilusao(True)
                self.ismovingright = True
                self.ismovingleft = False
                self.move(player.velocidadeplayerx)
            else:
                player.ilusao(False)
                self.ismovingright = False
        else:
            self.ismovingright = False

    def move(self, velocidade):

        for i in self.plataformas:
            i.move_x(velocidade * self.janela.delta_time())
        for j in self.chao:
            j.move_x(velocidade * self.janela.delta_time())

    def testaplataforma(self, jogador):

        if not jogador.sem_fisica:
            for i in self.plataformas:
                if i.x - jogador.player.width/2 < jogador.player.x < i.x + i.width - jogador.player.width/2:
                    if i.y <= jogador.player.y + jogador.player.height < i.y + i.height:
                        if jogador.velocidadey >= 0:
                            jogador.caiu(False, i.y)
                            jogador.in_ground = False
                            return

        for i in self.chao:
            if i.x - jogador.player.width/2 < jogador.player.x < i.x + i.width - jogador.player.width/2:
                if i.y <= jogador.player.y + jogador.player.height < i.y + 200:
                    jogador.caiu(False, i.y)
                    jogador.in_ground = True
                    return

        jogador.caiu(True)

    def render(self, jogador):

        for i in self.plataformas:
            if i.x < jogador.player.x + 800 or i.x + i.width > jogador.player.x - 800:
                i.draw()
        for i in self.chao:
            if i.x < jogador.player.x + 800 or i.x + i.width > jogador.player.x - 800:
                i.draw()

    def definelimite(self):

        maior = 0
        menor = 0
        for i in range(1, len(self.chao)):
            if self.chao[i].x > self.chao[maior].x:
                maior = i

            if self.chao[i].x < self.chao[menor].x:
                menor = i

        if self.chao[maior].x + self.chao[maior].width <= self.janela.width:
            self.limitedir = True
        else:
            self.limitedir = False

        if self.chao[menor].x >= 0:
            self.limiteesq = True
        else:
            self.limiteesq = False
