from PPlay.sprite import *
from PPlay.gameimage import *


class ControladorFundo:

    def __init__(self, janela, diretorio):

        self.janela = janela
        self.fundo0 = GameImage("{}/fundo0.png".format(diretorio))
        self.fundo1 = []
        self.fundo2 = []
        x = 0
        for i in range(3):
            self.fundo1.append(Sprite("{}/fundomeio_{}.png".format(diretorio, i)))
            self.fundo1[i].set_position(x, 0)
            self.fundo2.append(Sprite("{}/fundofrente_{}.png".format(diretorio, i)))
            self.fundo2[i].set_position(x, 0)
            x += 800

        self.janelaatual = 0
        self.proximajanela = 1

    def update(self, controladorplataformas, player):

        if controladorplataformas.ismovingleft:
            self.natural_move(-player.velocidadeplayerx)
        elif controladorplataformas.ismovingright:
            self.natural_move(player.velocidadeplayerx)

        if self.fundo1[self.proximajanela].x <= 0 and not controladorplataformas.limitedir:
            self.proximajanela += 1
            self.janelaatual += 1

        if self.fundo1[self.proximajanela].x >= self.janela.width and not controladorplataformas.limiteesq:
            self.proximajanela -= 1
            self.janelaatual -= 1

    def natural_move(self, velocidade):

        for i in self.fundo1:
            i.move_x((velocidade/4) * self.janela.delta_time())

        for j in self.fundo2:
            j.move_x((velocidade/3.5) * self.janela.delta_time())

    def render(self):

        self.fundo0.draw()
        self.fundo1[self.janelaatual].draw()
        self.fundo2[self.janelaatual].draw()
        self.fundo1[self.proximajanela].draw()
        self.fundo2[self.proximajanela].draw()
