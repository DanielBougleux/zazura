from Monstros.Arvore import *


class ControladorMonstros:

    def __init__(self, arquivo, controladorplataformas, player, janela):

        self.controladorplataformas = controladorplataformas
        self.player = player
        self.janela = janela

        self.monstros = []

        arq = open(arquivo, 'r')
        linhas = arq.readlines()
        arq.close()

        for i in linhas:
            i = i.split(" ")

            if int(i[0]) == 0:
                if int(i[1]) == 0:
                    self.monstros.append(Arvore(self.controladorplataformas.plataformas[int(i[2])], janela))

            if int(i[0]) == 1:
                if int(i[1]) == 0:
                    self.monstros.append(Arvore(self.controladorplataformas.chao[int(i[2])], janela))

    def update(self):

        for i in self.monstros:
            i.update(self.player, self.controladorplataformas)

    def render(self):

        for i in self.monstros:
            if 0 < i.colisor.x + i.colisor.width < self.janela.width:
                i.render()
