from Player.Dano import *
from random import randint


class ControladorCombate:

    def __init__(self, controladormonstros, player, janela, controlador_itens):

        self.controladormonstros = controladormonstros
        self.controlador_itens = controlador_itens
        self.player = player
        self.janela = janela
        self.dano = []

    def update(self):

        morto = -1
        for i in self.controladormonstros.monstros:
            if 0 < i.colisor.x + i.colisor.width < self.janela.width:
                if i.colisor.collided(self.player.colisor):
                    self.player.velocidadex = 0
                    if self.player.colisor.x < i.colisor.x:
                        self.player.set_position(self.player.colisor.x - 1, self.player.player.y)
                    else:
                        self.player.set_position(self.player.colisor.x + 1, self.player.player.y)
                if i.verifica_hit(self.player):
                    self.hitPlayer(self.player, i)
                if self.player.verifica_hit(i):
                    self.hitMonstro(self.player, i)
                if i.hp <= 0:
                    morto = self.controladormonstros.monstros.index(i)
                    drop = randint(0, 10)
                    if drop == 2 or drop == 5:
                        self.controlador_itens.add_item(i.colisor.x,
                                                        i.colisor.y + i.colisor.height)

        self.verifica_skill()

        if morto != -1:
            self.controladormonstros.monstros.pop(morto)

        index = -1
        for i in self.dano:
            i.update()
            if i.finished:
                index = self.dano.index(i)
        if index != -1:
            self.dano.pop(index)

    def hitPlayer(self, player, monstro):
        dano = monstro.danoataque - player.defesa
        if dano > 99:
            dano = 99
        player.hit(dano)
        self.dano.append(Dano(dano, player.player.x + 10, player.player.y + 20, self.janela))

    def hitMonstro(self, player, monstro):
        dano = player.danoataque - monstro.defesa
        if dano > 99:
            dano = 99
        monstro.hit(dano)
        self.dano.append(Dano(dano, monstro.monstro.x + 40, monstro.monstro.y + 40, self.janela))

    def verifica_skill(self):

        if self.player.espat or self.player.z:

            if self.player.esp.skill == "z":

                for i in self.controladormonstros.monstros:
                    if i.colisor.collided(self.player.esp.especial):
                        dano = self.player.esp.damage - i.defesa
                        if dano > 99:
                            dano = 99
                        i.hit(dano)
                        self.dano.append(Dano(dano, i.monstro.x + 40, i.monstro.y + 40, self.janela))
                        self.player.espat = False
                        self.player.z = False

            if self.player.esp.skill == "x":

                for i in self.controladormonstros.monstros:
                    if i.colisor.collided(self.player.esp.especial):
                        dano = self.player.esp.damage - i.defesa
                        if dano > 99:
                            dano = 99
                        i.hit(dano)
                        self.dano.append(Dano(dano, i.monstro.x + 40, i.monstro.y + 40, self.janela))

    def render(self):

        for i in self.dano:
            i.render()
