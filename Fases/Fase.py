from Controladores.ControladorFundo import *
from Controladores.ControladorPlataformas import *
from Controladores.ControladorMonstros import *
from Controladores.controladorCombate import *
from Controladores.ControladorItens import *


class Fase:

    def __init__(self, janela, player, numero_fase):

        self.janela = janela
        self.player = player
        self.fps = 0
        self.contador = 0
        self.tempo = 0

        self.controlador_plataforma = ControladorPlataformas(
            "Arquivos/Fase{}/arquivoplat.txt".format(numero_fase),
            "Arquivos/Fase{}/arquivochao.txt".format(numero_fase),
            "Graphics/Plataformas/Fase{}".format(numero_fase), self.janela)

        self.controlador_itens = ControladorItens(self.player, self.janela,
                                                  self.controlador_plataforma)

        self.controlador_fundo = ControladorFundo(self.janela,
                                                  "Graphics/Fundo/Fase{}".format(numero_fase))

        self.controladormonstros = ControladorMonstros(
            "Arquivos/Fase{}/arquivomonstros.txt".format(numero_fase),
            self.controlador_plataforma, self.player, self.janela)

        self.controladorcombate = ControladorCombate(self.controladormonstros, self.player,
                                                     self.janela, self.controlador_itens)

    def loop(self):

        while True:

            self.tempo += self.janela.delta_time()
            self.contador += 1

            if self.tempo >= 1:
                self.fps = self.contador
                self.contador = 0
                self.tempo = 0

            if self.player.player.x <= 0:
                self.player.velocidadex = 0
                self.player.set_position(self.player.colisor.x + 1, self.player.player.y)
            elif self.player.colisor.x + self.player.colisor.width >= self.janela.width:
                self.player.velocidadex = 0
                self.player.set_position(self.player.colisor.x - 1, self.player.player.y)

            self.controladorcombate.update()

            self.player.update()

            self.controlador_plataforma.testaplataforma(self.player)

            self.controlador_plataforma.update(self.player)
            self.controlador_fundo.update(self.controlador_plataforma, self.player)
            self.controladormonstros.update()
            self.controlador_itens.update()

            self.render()

            self.janela.update()

    def render(self):

        self.controlador_fundo.render()
        self.controlador_plataforma.render(self.player)
        if self.player.espat or self.player.z:
            self.player.espat = self.player.esp.update(self.player.z, self.player.espat)
            self.player.esp.render(self.player.z, self.player.espat)
        if not self.player.espat:
            self.player.render()
        self.player.desenha_cooldown()
        self.controladormonstros.render()
        self.controladorcombate.render()
        self.controlador_itens.render()
        self.janela.draw_text('FPS: ' + str(self.fps), 0, 0, 18, [0, 255, 255])
